def multiply(x, y):
    return x * y


def reverse_list(list):
    list.reverse()
    return list


def full_name(name, surname):
    return name + ' ' + surname


print(multiply(10, 5))
print(reverse_list([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]))
print(full_name('Reza', 'Latib'))

import unittest
import functions


class TestFunctions(unittest.TestCase):
    def test_multiply(self):
        result = functions.multiply(10, 5)
        self.assertEqual(result, 50)

    def test_reverse_list(self):
        result = functions.reverse_list([0, 1, 2, 3, 4, 5])
        self.assertEqual(result, [5, 4, 3, 2, 1, 0])

    def test_full_name(self):
        result = functions.full_name('Reza', 'Latib')
        self.assertEqual(result, 'Reza Latib')
